/* MpegTsEnums.c generated by valac 0.34.4, the Vala compiler
 * generated from MpegTsEnums.vala, do not modify */

/*
 * Copyright (C) 2008,2009 Sebastian Pölsterl
 *
 * This file is part of GNOME DVB Daemon.
 *
 * GNOME DVB Daemon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNOME DVB Daemon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNOME DVB Daemon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <gst/mpegts/mpegts.h>


#define DVB_TYPE_DVB_SRC_DELSYS (dvb_dvb_src_delsys_get_type ())

#define DVB_TYPE_ADAPTER_TYPE (dvb_adapter_type_get_type ())

typedef enum  {
	DVB_DVB_SRC_DELSYS_SYS_UNDEFINED,
	DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_A,
	DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_B,
	DVB_DVB_SRC_DELSYS_SYS_DVBT,
	DVB_DVB_SRC_DELSYS_SYS_DSS,
	DVB_DVB_SRC_DELSYS_SYS_DVBS,
	DVB_DVB_SRC_DELSYS_SYS_DVBS2,
	DVB_DVB_SRC_DELSYS_SYS_DVBH,
	DVB_DVB_SRC_DELSYS_SYS_ISDBT,
	DVB_DVB_SRC_DELSYS_SYS_ISDBS,
	DVB_DVB_SRC_DELSYS_SYS_ISDBC,
	DVB_DVB_SRC_DELSYS_SYS_ATSC,
	DVB_DVB_SRC_DELSYS_SYS_ATSCMH,
	DVB_DVB_SRC_DELSYS_SYS_DTMB,
	DVB_DVB_SRC_DELSYS_SYS_CMMB,
	DVB_DVB_SRC_DELSYS_SYS_DAB,
	DVB_DVB_SRC_DELSYS_SYS_DVBT2,
	DVB_DVB_SRC_DELSYS_SYS_TURBO,
	DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_C
} DVBDvbSrcDelsys;

typedef enum  {
	DVB_ADAPTER_TYPE_UNKNOWN,
	DVB_ADAPTER_TYPE_TERRESTRIAL,
	DVB_ADAPTER_TYPE_SATELLITE,
	DVB_ADAPTER_TYPE_CABLE
} DVBAdapterType;



GType dvb_dvb_src_delsys_get_type (void) G_GNUC_CONST;
GstMpegtsDVBCodeRate dvb_getCodeRateEnum (const gchar* val);
gchar* dvb_getCodeRateString (GstMpegtsDVBCodeRate val);
GstMpegtsModulationType dvb_getModulationEnum (const gchar* val);
gchar* dvb_getModulationString (GstMpegtsModulationType val);
GstMpegtsTerrestrialGuardInterval dvb_getGuardIntervalEnum (const gchar* val);
gchar* dvb_getGuardIntervalString (GstMpegtsTerrestrialGuardInterval val);
GstMpegtsTerrestrialHierarchy dvb_getHierarchyEnum (const gchar* val);
gchar* dvb_getHierarchyString (GstMpegtsTerrestrialHierarchy val);
GstMpegtsTerrestrialTransmissionMode dvb_getTransmissionModeEnum (const gchar* val);
gchar* dvb_getTransmissionModeString (GstMpegtsTerrestrialTransmissionMode val);
GstMpegtsSatellitePolarizationType dvb_getPolarizationEnum (const gchar* val);
gchar* dvb_getPolarizationString (GstMpegtsSatellitePolarizationType val);
GType dvb_adapter_type_get_type (void) G_GNUC_CONST;
gboolean dvb_isSupported (DVBDvbSrcDelsys delsys, DVBAdapterType type);


GType dvb_dvb_src_delsys_get_type (void) {
	static volatile gsize dvb_dvb_src_delsys_type_id__volatile = 0;
	if (g_once_init_enter (&dvb_dvb_src_delsys_type_id__volatile)) {
		static const GEnumValue values[] = {{DVB_DVB_SRC_DELSYS_SYS_UNDEFINED, "DVB_DVB_SRC_DELSYS_SYS_UNDEFINED", "sys-undefined"}, {DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_A, "DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_A", "sys-dvbc-annex-a"}, {DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_B, "DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_B", "sys-dvbc-annex-b"}, {DVB_DVB_SRC_DELSYS_SYS_DVBT, "DVB_DVB_SRC_DELSYS_SYS_DVBT", "sys-dvbt"}, {DVB_DVB_SRC_DELSYS_SYS_DSS, "DVB_DVB_SRC_DELSYS_SYS_DSS", "sys-dss"}, {DVB_DVB_SRC_DELSYS_SYS_DVBS, "DVB_DVB_SRC_DELSYS_SYS_DVBS", "sys-dvbs"}, {DVB_DVB_SRC_DELSYS_SYS_DVBS2, "DVB_DVB_SRC_DELSYS_SYS_DVBS2", "sys-dvbs2"}, {DVB_DVB_SRC_DELSYS_SYS_DVBH, "DVB_DVB_SRC_DELSYS_SYS_DVBH", "sys-dvbh"}, {DVB_DVB_SRC_DELSYS_SYS_ISDBT, "DVB_DVB_SRC_DELSYS_SYS_ISDBT", "sys-isdbt"}, {DVB_DVB_SRC_DELSYS_SYS_ISDBS, "DVB_DVB_SRC_DELSYS_SYS_ISDBS", "sys-isdbs"}, {DVB_DVB_SRC_DELSYS_SYS_ISDBC, "DVB_DVB_SRC_DELSYS_SYS_ISDBC", "sys-isdbc"}, {DVB_DVB_SRC_DELSYS_SYS_ATSC, "DVB_DVB_SRC_DELSYS_SYS_ATSC", "sys-atsc"}, {DVB_DVB_SRC_DELSYS_SYS_ATSCMH, "DVB_DVB_SRC_DELSYS_SYS_ATSCMH", "sys-atscmh"}, {DVB_DVB_SRC_DELSYS_SYS_DTMB, "DVB_DVB_SRC_DELSYS_SYS_DTMB", "sys-dtmb"}, {DVB_DVB_SRC_DELSYS_SYS_CMMB, "DVB_DVB_SRC_DELSYS_SYS_CMMB", "sys-cmmb"}, {DVB_DVB_SRC_DELSYS_SYS_DAB, "DVB_DVB_SRC_DELSYS_SYS_DAB", "sys-dab"}, {DVB_DVB_SRC_DELSYS_SYS_DVBT2, "DVB_DVB_SRC_DELSYS_SYS_DVBT2", "sys-dvbt2"}, {DVB_DVB_SRC_DELSYS_SYS_TURBO, "DVB_DVB_SRC_DELSYS_SYS_TURBO", "sys-turbo"}, {DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_C, "DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_C", "sys-dvbc-annex-c"}, {0, NULL, NULL}};
		GType dvb_dvb_src_delsys_type_id;
		dvb_dvb_src_delsys_type_id = g_enum_register_static ("DVBDvbSrcDelsys", values);
		g_once_init_leave (&dvb_dvb_src_delsys_type_id__volatile, dvb_dvb_src_delsys_type_id);
	}
	return dvb_dvb_src_delsys_type_id__volatile;
}


GstMpegtsDVBCodeRate dvb_getCodeRateEnum (const gchar* val) {
	GstMpegtsDVBCodeRate result = 0;
	const gchar* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	static GQuark _tmp2_label4 = 0;
	static GQuark _tmp2_label5 = 0;
	static GQuark _tmp2_label6 = 0;
	static GQuark _tmp2_label7 = 0;
	static GQuark _tmp2_label8 = 0;
	static GQuark _tmp2_label9 = 0;
	static GQuark _tmp2_label10 = 0;
	static GQuark _tmp2_label11 = 0;
	g_return_val_if_fail (val != NULL, 0);
	_tmp0_ = val;
	_tmp1_ = _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("1/2")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_1_2;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("2/3")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_2_3;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("2/5")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_2_5;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("3/4")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_3_4;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label4) ? _tmp2_label4 : (_tmp2_label4 = g_quark_from_static_string ("3/5")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_3_5;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label5) ? _tmp2_label5 : (_tmp2_label5 = g_quark_from_static_string ("4/5")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_4_5;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label6) ? _tmp2_label6 : (_tmp2_label6 = g_quark_from_static_string ("5/6")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_5_6;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label7) ? _tmp2_label7 : (_tmp2_label7 = g_quark_from_static_string ("6/7")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_6_7;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label8) ? _tmp2_label8 : (_tmp2_label8 = g_quark_from_static_string ("7/8")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_7_8;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label9) ? _tmp2_label9 : (_tmp2_label9 = g_quark_from_static_string ("8/9")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_8_9;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label10) ? _tmp2_label10 : (_tmp2_label10 = g_quark_from_static_string ("9/10")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_9_10;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label11) ? _tmp2_label11 : (_tmp2_label11 = g_quark_from_static_string ("NONE")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_NONE;
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_FEC_AUTO;
				return result;
			}
		}
	}
}


gchar* dvb_getCodeRateString (GstMpegtsDVBCodeRate val) {
	gchar* result = NULL;
	GstMpegtsDVBCodeRate _tmp0_ = 0;
	_tmp0_ = val;
	switch (_tmp0_) {
		case GST_MPEGTS_FEC_1_2:
		{
			gchar* _tmp1_ = NULL;
			_tmp1_ = g_strdup ("1/2");
			result = _tmp1_;
			return result;
		}
		case GST_MPEGTS_FEC_2_3:
		{
			gchar* _tmp2_ = NULL;
			_tmp2_ = g_strdup ("2/3");
			result = _tmp2_;
			return result;
		}
		case GST_MPEGTS_FEC_2_5:
		{
			gchar* _tmp3_ = NULL;
			_tmp3_ = g_strdup ("2/5");
			result = _tmp3_;
			return result;
		}
		case GST_MPEGTS_FEC_3_4:
		{
			gchar* _tmp4_ = NULL;
			_tmp4_ = g_strdup ("3/4");
			result = _tmp4_;
			return result;
		}
		case GST_MPEGTS_FEC_3_5:
		{
			gchar* _tmp5_ = NULL;
			_tmp5_ = g_strdup ("3/5");
			result = _tmp5_;
			return result;
		}
		case GST_MPEGTS_FEC_4_5:
		{
			gchar* _tmp6_ = NULL;
			_tmp6_ = g_strdup ("4/5");
			result = _tmp6_;
			return result;
		}
		case GST_MPEGTS_FEC_5_6:
		{
			gchar* _tmp7_ = NULL;
			_tmp7_ = g_strdup ("5/6");
			result = _tmp7_;
			return result;
		}
		case GST_MPEGTS_FEC_6_7:
		{
			gchar* _tmp8_ = NULL;
			_tmp8_ = g_strdup ("6/7");
			result = _tmp8_;
			return result;
		}
		case GST_MPEGTS_FEC_7_8:
		{
			gchar* _tmp9_ = NULL;
			_tmp9_ = g_strdup ("7/8");
			result = _tmp9_;
			return result;
		}
		case GST_MPEGTS_FEC_8_9:
		{
			gchar* _tmp10_ = NULL;
			_tmp10_ = g_strdup ("8/9");
			result = _tmp10_;
			return result;
		}
		case GST_MPEGTS_FEC_9_10:
		{
			gchar* _tmp11_ = NULL;
			_tmp11_ = g_strdup ("9/10");
			result = _tmp11_;
			return result;
		}
		case GST_MPEGTS_FEC_NONE:
		{
			gchar* _tmp12_ = NULL;
			_tmp12_ = g_strdup ("NONE");
			result = _tmp12_;
			return result;
		}
		default:
		case GST_MPEGTS_FEC_AUTO:
		{
			gchar* _tmp13_ = NULL;
			_tmp13_ = g_strdup ("AUTO");
			result = _tmp13_;
			return result;
		}
	}
}


GstMpegtsModulationType dvb_getModulationEnum (const gchar* val) {
	GstMpegtsModulationType result = 0;
	const gchar* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	static GQuark _tmp2_label4 = 0;
	static GQuark _tmp2_label5 = 0;
	static GQuark _tmp2_label6 = 0;
	static GQuark _tmp2_label7 = 0;
	static GQuark _tmp2_label8 = 0;
	static GQuark _tmp2_label9 = 0;
	static GQuark _tmp2_label10 = 0;
	static GQuark _tmp2_label11 = 0;
	static GQuark _tmp2_label12 = 0;
	static GQuark _tmp2_label13 = 0;
	g_return_val_if_fail (val != NULL, 0);
	_tmp0_ = val;
	_tmp1_ = _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("QPSK")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QPSK;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("QAM/16")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_16;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("QAM/32")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_32;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("QAM/64")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_64;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label4) ? _tmp2_label4 : (_tmp2_label4 = g_quark_from_static_string ("QAM/128")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_128;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label5) ? _tmp2_label5 : (_tmp2_label5 = g_quark_from_static_string ("QAM/256")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_256;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label6) ? _tmp2_label6 : (_tmp2_label6 = g_quark_from_static_string ("QAM/AUTO")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_AUTO;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label7) ? _tmp2_label7 : (_tmp2_label7 = g_quark_from_static_string ("VSB/8")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_VSB_8;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label8) ? _tmp2_label8 : (_tmp2_label8 = g_quark_from_static_string ("VSB/16")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_VSB_16;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label9) ? _tmp2_label9 : (_tmp2_label9 = g_quark_from_static_string ("PSK/8")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_PSK_8;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label10) ? _tmp2_label10 : (_tmp2_label10 = g_quark_from_static_string ("APSK/16")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_APSK_16;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label11) ? _tmp2_label11 : (_tmp2_label11 = g_quark_from_static_string ("APSK/32")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_APSK_32;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label12) ? _tmp2_label12 : (_tmp2_label12 = g_quark_from_static_string ("DQPSK")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_DQPSK;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label13) ? _tmp2_label13 : (_tmp2_label13 = g_quark_from_static_string ("QAM/4_NR")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_4_NR_;
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_MODULATION_QAM_AUTO;
				return result;
			}
		}
	}
}


gchar* dvb_getModulationString (GstMpegtsModulationType val) {
	gchar* result = NULL;
	GstMpegtsModulationType _tmp0_ = 0;
	_tmp0_ = val;
	switch (_tmp0_) {
		case GST_MPEGTS_MODULATION_QPSK:
		{
			gchar* _tmp1_ = NULL;
			_tmp1_ = g_strdup ("QPSK");
			result = _tmp1_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_16:
		{
			gchar* _tmp2_ = NULL;
			_tmp2_ = g_strdup ("QAM/16");
			result = _tmp2_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_32:
		{
			gchar* _tmp3_ = NULL;
			_tmp3_ = g_strdup ("QAM/32");
			result = _tmp3_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_64:
		{
			gchar* _tmp4_ = NULL;
			_tmp4_ = g_strdup ("QAM/64");
			result = _tmp4_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_128:
		{
			gchar* _tmp5_ = NULL;
			_tmp5_ = g_strdup ("QAM/128");
			result = _tmp5_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_256:
		{
			gchar* _tmp6_ = NULL;
			_tmp6_ = g_strdup ("QAM/256");
			result = _tmp6_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_AUTO:
		{
			gchar* _tmp7_ = NULL;
			_tmp7_ = g_strdup ("QAM/AUTO");
			result = _tmp7_;
			return result;
		}
		case GST_MPEGTS_MODULATION_VSB_8:
		{
			gchar* _tmp8_ = NULL;
			_tmp8_ = g_strdup ("VSB/8");
			result = _tmp8_;
			return result;
		}
		case GST_MPEGTS_MODULATION_VSB_16:
		{
			gchar* _tmp9_ = NULL;
			_tmp9_ = g_strdup ("VSB/16");
			result = _tmp9_;
			return result;
		}
		case GST_MPEGTS_MODULATION_PSK_8:
		{
			gchar* _tmp10_ = NULL;
			_tmp10_ = g_strdup ("PSK/8");
			result = _tmp10_;
			return result;
		}
		case GST_MPEGTS_MODULATION_APSK_16:
		{
			gchar* _tmp11_ = NULL;
			_tmp11_ = g_strdup ("APSK/16");
			result = _tmp11_;
			return result;
		}
		case GST_MPEGTS_MODULATION_APSK_32:
		{
			gchar* _tmp12_ = NULL;
			_tmp12_ = g_strdup ("APSK/32");
			result = _tmp12_;
			return result;
		}
		case GST_MPEGTS_MODULATION_DQPSK:
		{
			gchar* _tmp13_ = NULL;
			_tmp13_ = g_strdup ("DQPSK");
			result = _tmp13_;
			return result;
		}
		case GST_MPEGTS_MODULATION_QAM_4_NR_:
		{
			gchar* _tmp14_ = NULL;
			_tmp14_ = g_strdup ("QAM/4_NR");
			result = _tmp14_;
			return result;
		}
		default:
		{
			gchar* _tmp15_ = NULL;
			_tmp15_ = g_strdup ("QAM/AUTO");
			result = _tmp15_;
			return result;
		}
	}
}


GstMpegtsTerrestrialGuardInterval dvb_getGuardIntervalEnum (const gchar* val) {
	GstMpegtsTerrestrialGuardInterval result = 0;
	const gchar* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	static GQuark _tmp2_label4 = 0;
	static GQuark _tmp2_label5 = 0;
	static GQuark _tmp2_label6 = 0;
	static GQuark _tmp2_label7 = 0;
	static GQuark _tmp2_label8 = 0;
	static GQuark _tmp2_label9 = 0;
	static GQuark _tmp2_label10 = 0;
	g_return_val_if_fail (val != NULL, 0);
	_tmp0_ = val;
	_tmp1_ = _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("1/32")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_1_32;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("1/16")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_1_16;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("1/8")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_1_8;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("1/4")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_1_4;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label4) ? _tmp2_label4 : (_tmp2_label4 = g_quark_from_static_string ("AUTO")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_AUTO;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label5) ? _tmp2_label5 : (_tmp2_label5 = g_quark_from_static_string ("1/128")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_1_128;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label6) ? _tmp2_label6 : (_tmp2_label6 = g_quark_from_static_string ("19/128")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_19_128;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label7) ? _tmp2_label7 : (_tmp2_label7 = g_quark_from_static_string ("19/256")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_19_256;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label8) ? _tmp2_label8 : (_tmp2_label8 = g_quark_from_static_string ("PN420")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_PN420;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label9) ? _tmp2_label9 : (_tmp2_label9 = g_quark_from_static_string ("PN595")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_PN595;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label10) ? _tmp2_label10 : (_tmp2_label10 = g_quark_from_static_string ("PN945")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_PN945;
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_GUARD_INTERVAL_AUTO;
				return result;
			}
		}
	}
}


gchar* dvb_getGuardIntervalString (GstMpegtsTerrestrialGuardInterval val) {
	gchar* result = NULL;
	GstMpegtsTerrestrialGuardInterval _tmp0_ = 0;
	_tmp0_ = val;
	switch (_tmp0_) {
		case GST_MPEGTS_GUARD_INTERVAL_1_32:
		{
			gchar* _tmp1_ = NULL;
			_tmp1_ = g_strdup ("1/32");
			result = _tmp1_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_1_16:
		{
			gchar* _tmp2_ = NULL;
			_tmp2_ = g_strdup ("1/16");
			result = _tmp2_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_1_8:
		{
			gchar* _tmp3_ = NULL;
			_tmp3_ = g_strdup ("1/8");
			result = _tmp3_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_1_4:
		{
			gchar* _tmp4_ = NULL;
			_tmp4_ = g_strdup ("1/4");
			result = _tmp4_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_AUTO:
		{
			gchar* _tmp5_ = NULL;
			_tmp5_ = g_strdup ("AUTO");
			result = _tmp5_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_1_128:
		{
			gchar* _tmp6_ = NULL;
			_tmp6_ = g_strdup ("1/128");
			result = _tmp6_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_19_128:
		{
			gchar* _tmp7_ = NULL;
			_tmp7_ = g_strdup ("19/128");
			result = _tmp7_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_19_256:
		{
			gchar* _tmp8_ = NULL;
			_tmp8_ = g_strdup ("19/256");
			result = _tmp8_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_PN420:
		{
			gchar* _tmp9_ = NULL;
			_tmp9_ = g_strdup ("PN420");
			result = _tmp9_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_PN595:
		{
			gchar* _tmp10_ = NULL;
			_tmp10_ = g_strdup ("PN595");
			result = _tmp10_;
			return result;
		}
		case GST_MPEGTS_GUARD_INTERVAL_PN945:
		{
			gchar* _tmp11_ = NULL;
			_tmp11_ = g_strdup ("PN945");
			result = _tmp11_;
			return result;
		}
		default:
		{
			gchar* _tmp12_ = NULL;
			_tmp12_ = g_strdup ("AUTO");
			result = _tmp12_;
			return result;
		}
	}
}


GstMpegtsTerrestrialHierarchy dvb_getHierarchyEnum (const gchar* val) {
	GstMpegtsTerrestrialHierarchy result = 0;
	const gchar* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	g_return_val_if_fail (val != NULL, 0);
	_tmp0_ = val;
	_tmp1_ = _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("NONE")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_HIERARCHY_NONE;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("1")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_HIERARCHY_1;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("2")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_HIERARCHY_2;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("4")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_HIERARCHY_4;
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_HIERARCHY_AUTO;
				return result;
			}
		}
	}
}


gchar* dvb_getHierarchyString (GstMpegtsTerrestrialHierarchy val) {
	gchar* result = NULL;
	GstMpegtsTerrestrialHierarchy _tmp0_ = 0;
	_tmp0_ = val;
	switch (_tmp0_) {
		case GST_MPEGTS_HIERARCHY_NONE:
		{
			gchar* _tmp1_ = NULL;
			_tmp1_ = g_strdup ("NONE");
			result = _tmp1_;
			return result;
		}
		case GST_MPEGTS_HIERARCHY_1:
		{
			gchar* _tmp2_ = NULL;
			_tmp2_ = g_strdup ("1");
			result = _tmp2_;
			return result;
		}
		case GST_MPEGTS_HIERARCHY_2:
		{
			gchar* _tmp3_ = NULL;
			_tmp3_ = g_strdup ("2");
			result = _tmp3_;
			return result;
		}
		case GST_MPEGTS_HIERARCHY_4:
		{
			gchar* _tmp4_ = NULL;
			_tmp4_ = g_strdup ("4");
			result = _tmp4_;
			return result;
		}
		default:
		case GST_MPEGTS_HIERARCHY_AUTO:
		{
			gchar* _tmp5_ = NULL;
			_tmp5_ = g_strdup ("AUTO");
			result = _tmp5_;
			return result;
		}
	}
}


GstMpegtsTerrestrialTransmissionMode dvb_getTransmissionModeEnum (const gchar* val) {
	GstMpegtsTerrestrialTransmissionMode result = 0;
	const gchar* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	static GQuark _tmp2_label4 = 0;
	static GQuark _tmp2_label5 = 0;
	static GQuark _tmp2_label6 = 0;
	static GQuark _tmp2_label7 = 0;
	static GQuark _tmp2_label8 = 0;
	g_return_val_if_fail (val != NULL, 0);
	_tmp0_ = val;
	_tmp1_ = _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("2K")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_2K;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("8K")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_8K;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("AUTO")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_AUTO;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("4K")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_4K;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label4) ? _tmp2_label4 : (_tmp2_label4 = g_quark_from_static_string ("1K")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_1K;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label5) ? _tmp2_label5 : (_tmp2_label5 = g_quark_from_static_string ("16K")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_16K;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label6) ? _tmp2_label6 : (_tmp2_label6 = g_quark_from_static_string ("32K")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_32K;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label7) ? _tmp2_label7 : (_tmp2_label7 = g_quark_from_static_string ("C1")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_C1;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label8) ? _tmp2_label8 : (_tmp2_label8 = g_quark_from_static_string ("C3780")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_C3780;
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_TRANSMISSION_MODE_AUTO;
				return result;
			}
		}
	}
}


gchar* dvb_getTransmissionModeString (GstMpegtsTerrestrialTransmissionMode val) {
	gchar* result = NULL;
	GstMpegtsTerrestrialTransmissionMode _tmp0_ = 0;
	_tmp0_ = val;
	switch (_tmp0_) {
		case GST_MPEGTS_TRANSMISSION_MODE_2K:
		{
			gchar* _tmp1_ = NULL;
			_tmp1_ = g_strdup ("2K");
			result = _tmp1_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_8K:
		{
			gchar* _tmp2_ = NULL;
			_tmp2_ = g_strdup ("8K");
			result = _tmp2_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_AUTO:
		{
			gchar* _tmp3_ = NULL;
			_tmp3_ = g_strdup ("AUTO");
			result = _tmp3_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_4K:
		{
			gchar* _tmp4_ = NULL;
			_tmp4_ = g_strdup ("4K");
			result = _tmp4_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_1K:
		{
			gchar* _tmp5_ = NULL;
			_tmp5_ = g_strdup ("1K");
			result = _tmp5_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_16K:
		{
			gchar* _tmp6_ = NULL;
			_tmp6_ = g_strdup ("16K");
			result = _tmp6_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_32K:
		{
			gchar* _tmp7_ = NULL;
			_tmp7_ = g_strdup ("32K");
			result = _tmp7_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_C1:
		{
			gchar* _tmp8_ = NULL;
			_tmp8_ = g_strdup ("C1");
			result = _tmp8_;
			return result;
		}
		case GST_MPEGTS_TRANSMISSION_MODE_C3780:
		{
			gchar* _tmp9_ = NULL;
			_tmp9_ = g_strdup ("C3780");
			result = _tmp9_;
			return result;
		}
		default:
		{
			gchar* _tmp10_ = NULL;
			_tmp10_ = g_strdup ("AUTO");
			result = _tmp10_;
			return result;
		}
	}
}


GstMpegtsSatellitePolarizationType dvb_getPolarizationEnum (const gchar* val) {
	GstMpegtsSatellitePolarizationType result = 0;
	const gchar* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	GQuark _tmp3_ = 0U;
	static GQuark _tmp2_label0 = 0;
	static GQuark _tmp2_label1 = 0;
	static GQuark _tmp2_label2 = 0;
	static GQuark _tmp2_label3 = 0;
	g_return_val_if_fail (val != NULL, 0);
	_tmp0_ = val;
	_tmp1_ = _tmp0_;
	_tmp3_ = (NULL == _tmp1_) ? 0 : g_quark_from_string (_tmp1_);
	if (_tmp3_ == ((0 != _tmp2_label0) ? _tmp2_label0 : (_tmp2_label0 = g_quark_from_static_string ("VERTICAL")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_POLARIZATION_LINEAR_VERTICAL;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label1) ? _tmp2_label1 : (_tmp2_label1 = g_quark_from_static_string ("HORIZONTAL")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_POLARIZATION_LINEAR_HORIZONTAL;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label2) ? _tmp2_label2 : (_tmp2_label2 = g_quark_from_static_string ("LEFT")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_POLARIZATION_CIRCULAR_LEFT;
				return result;
			}
		}
	} else if (_tmp3_ == ((0 != _tmp2_label3) ? _tmp2_label3 : (_tmp2_label3 = g_quark_from_static_string ("RIGHT")))) {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_POLARIZATION_CIRCULAR_RIGHT;
				return result;
			}
		}
	} else {
		switch (0) {
			default:
			{
				result = GST_MPEGTS_POLARIZATION_LINEAR_VERTICAL;
				return result;
			}
		}
	}
}


gchar* dvb_getPolarizationString (GstMpegtsSatellitePolarizationType val) {
	gchar* result = NULL;
	GstMpegtsSatellitePolarizationType _tmp0_ = 0;
	_tmp0_ = val;
	switch (_tmp0_) {
		case GST_MPEGTS_POLARIZATION_LINEAR_VERTICAL:
		{
			gchar* _tmp1_ = NULL;
			_tmp1_ = g_strdup ("VERTICAL");
			result = _tmp1_;
			return result;
		}
		case GST_MPEGTS_POLARIZATION_LINEAR_HORIZONTAL:
		{
			gchar* _tmp2_ = NULL;
			_tmp2_ = g_strdup ("HORIZONTAL");
			result = _tmp2_;
			return result;
		}
		case GST_MPEGTS_POLARIZATION_CIRCULAR_LEFT:
		{
			gchar* _tmp3_ = NULL;
			_tmp3_ = g_strdup ("LEFT");
			result = _tmp3_;
			return result;
		}
		case GST_MPEGTS_POLARIZATION_CIRCULAR_RIGHT:
		{
			gchar* _tmp4_ = NULL;
			_tmp4_ = g_strdup ("RIGHT");
			result = _tmp4_;
			return result;
		}
		default:
		{
			gchar* _tmp5_ = NULL;
			_tmp5_ = g_strdup ("VERTICAL");
			result = _tmp5_;
			return result;
		}
	}
}


gboolean dvb_isSupported (DVBDvbSrcDelsys delsys, DVBAdapterType type) {
	gboolean result = FALSE;
	DVBDvbSrcDelsys _tmp0_ = 0;
	_tmp0_ = delsys;
	switch (_tmp0_) {
		case DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_A:
		case DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_B:
		case DVB_DVB_SRC_DELSYS_SYS_DVBC_ANNEX_C:
		case DVB_DVB_SRC_DELSYS_SYS_ISDBC:
		{
			DVBAdapterType _tmp1_ = 0;
			_tmp1_ = type;
			result = _tmp1_ == DVB_ADAPTER_TYPE_CABLE;
			return result;
		}
		case DVB_DVB_SRC_DELSYS_SYS_DVBT:
		case DVB_DVB_SRC_DELSYS_SYS_DVBH:
		case DVB_DVB_SRC_DELSYS_SYS_ISDBT:
		case DVB_DVB_SRC_DELSYS_SYS_ATSC:
		case DVB_DVB_SRC_DELSYS_SYS_ATSCMH:
		case DVB_DVB_SRC_DELSYS_SYS_DVBT2:
		{
			DVBAdapterType _tmp2_ = 0;
			_tmp2_ = type;
			result = _tmp2_ == DVB_ADAPTER_TYPE_TERRESTRIAL;
			return result;
		}
		case DVB_DVB_SRC_DELSYS_SYS_DSS:
		case DVB_DVB_SRC_DELSYS_SYS_DVBS:
		case DVB_DVB_SRC_DELSYS_SYS_DVBS2:
		case DVB_DVB_SRC_DELSYS_SYS_TURBO:
		case DVB_DVB_SRC_DELSYS_SYS_ISDBS:
		{
			DVBAdapterType _tmp3_ = 0;
			_tmp3_ = type;
			result = _tmp3_ == DVB_ADAPTER_TYPE_SATELLITE;
			return result;
		}
		default:
		case DVB_DVB_SRC_DELSYS_SYS_DTMB:
		case DVB_DVB_SRC_DELSYS_SYS_CMMB:
		case DVB_DVB_SRC_DELSYS_SYS_DAB:
		case DVB_DVB_SRC_DELSYS_SYS_UNDEFINED:
		{
			result = FALSE;
			return result;
		}
	}
}



